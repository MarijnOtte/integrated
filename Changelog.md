## 0.2.1 ##

Other
<ul>
<li>Fixed dependency problem</li>
</ul>

## 0.2.0 ##

Bug
<ul>
<li>[<a href='https://eactive.atlassian.net/browse/INTEGRATED-19'>INTEGRATED-19</a>] -         Bug intro text item on home- and search page
</li>
<li>[<a href='https://eactive.atlassian.net/browse/INTEGRATED-23'>INTEGRATED-23</a>] -         Support empty date
</li>
<li>[<a href='https://eactive.atlassian.net/browse/INTEGRATED-31'>INTEGRATED-31</a>] -         Lock doesn&#39;t get remove after save
</li>
<li>[<a href='https://eactive.atlassian.net/browse/INTEGRATED-32'>INTEGRATED-32</a>] -         Solr index doesn&#39;t update immediately after saving an item
</li>
<li>[<a href='https://eactive.atlassian.net/browse/INTEGRATED-47'>INTEGRATED-47</a>] -         Solr fullindex doesn&#39;t index everything
</li>
<li>[<a href='https://eactive.atlassian.net/browse/INTEGRATED-57'>INTEGRATED-57</a>] -         &quot;Contenttypes&quot; spelling is wrong, should be &quot;Content types&quot;
</li>
<li>[<a href='https://eactive.atlassian.net/browse/INTEGRATED-69'>INTEGRATED-69</a>] -         Confirmation message isn&#39;t translated
</li>
<li>[<a href='https://eactive.atlassian.net/browse/INTEGRATED-71'>INTEGRATED-71</a>] -         Search button doesn&#39;t work
</li>
<li>[<a href='https://eactive.atlassian.net/browse/INTEGRATED-100'>INTEGRATED-100</a>] -         Translate login error messages
</li>
</ul>

Improvement
<ul>
<li>[<a href='https://eactive.atlassian.net/browse/INTEGRATED-16'>INTEGRATED-16</a>] -         Sort option search results
</li>
<li>[<a href='https://eactive.atlassian.net/browse/INTEGRATED-18'>INTEGRATED-18</a>] -         Locking and display editor name when editing item
</li>
<li>[<a href='https://eactive.atlassian.net/browse/INTEGRATED-20'>INTEGRATED-20</a>] -         Show intro (when available) on search page
</li>
<li>[<a href='https://eactive.atlassian.net/browse/INTEGRATED-37'>INTEGRATED-37</a>] -         Show profile image in header
</li>
<li>[<a href='https://eactive.atlassian.net/browse/INTEGRATED-42'>INTEGRATED-42</a>] -         Implement new layout for edit page
</li>
<li>[<a href='https://eactive.atlassian.net/browse/INTEGRATED-52'>INTEGRATED-52</a>] -         Show linked items from other side
</li>
<li>[<a href='https://eactive.atlassian.net/browse/INTEGRATED-54'>INTEGRATED-54</a>] -         Locking - install readme is not complete
</li>
<li>[<a href='https://eactive.atlassian.net/browse/INTEGRATED-65'>INTEGRATED-65</a>] -         Create user as in wireframes
</li>
<li>[<a href='https://eactive.atlassian.net/browse/INTEGRATED-66'>INTEGRATED-66</a>] -         Make sorting more intuitive
</li>
<li>[<a href='https://eactive.atlassian.net/browse/INTEGRATED-68'>INTEGRATED-68</a>] -         updatedAt column should be filled
</li>
<li>[<a href='https://eactive.atlassian.net/browse/INTEGRATED-72'>INTEGRATED-72</a>] -         publishAt column should be filled
</li>
<li>[<a href='https://eactive.atlassian.net/browse/INTEGRATED-73'>INTEGRATED-73</a>] -         Show publication date / updated date on homepage
</li>
</ul>
    
New Feature
<ul>
<li>[<a href='https://eactive.atlassian.net/browse/INTEGRATED-34'>INTEGRATED-34</a>] -         Add publishUntil field for editing
</li>
<li>[<a href='https://eactive.atlassian.net/browse/INTEGRATED-36'>INTEGRATED-36</a>] -         Add tinymce editor
</li>
</ul>
    